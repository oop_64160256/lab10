package com.nipitpon.week10;

public class Circle extends Shape {
    private double radius;

    public Circle(double radius) {
        super("Circle");
        this.radius = radius;
    }

    @Override
    public double calArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double calPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return this.getName() + " radius: " + this.radius;
    }
}
