package com.nipitpon.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectacgle rec = new Rectacgle(5, 3);
        System.out.println(rec);
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectacgle rec2 = new Rectacgle(2, 2);
        System.out.println(rec2);
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%.3f \n", circle1.calArea());
        System.out.printf("%.3f \n", circle1.calPerimeter());
        
        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%.3f \n", circle2.calArea());
        System.out.printf("%.3f \n", circle2.calPerimeter());

        Triangle tri1 = new Triangle(4, 5, 6);
        System.out.println(tri1);
        System.out.printf("%.3f \n", tri1.calArea());
        System.out.printf("%.3f \n", tri1.calPerimeter());

        Triangle tri2 = new Triangle(6, 9, 13);
        System.out.println(tri2);
        System.out.printf("%.3f \n", tri2.calArea());
        System.out.printf("%.3f \n", tri2.calPerimeter());
    }
}
