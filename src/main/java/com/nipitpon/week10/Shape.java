package com.nipitpon.week10;

public abstract class Shape {
    private String name;

    public Shape(String name) {
        this.name = name;

    }

    public abstract double calArea();
    

    public abstract double calPerimeter();

    public String getName() {
        return name;
    }
}
