package com.nipitpon.week10;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;
    private double parimeter;
    private double s;
    private double area;

    public Triangle(double a, double b, double c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
        this.parimeter = a+b+c; 
        this.s = parimeter/2;
        this.area = Math.sqrt((s*(s-a)*(s-b)*(s-c)));
    }

    @Override
    public double calArea() {
        return area;
    }

    @Override
    public double calPerimeter() {
        return parimeter;
    }

    @Override
    public String toString() {
        return this.getName() + " a: " + this.a + " b: " + this.b + " c: " + this.c;
    }
}
